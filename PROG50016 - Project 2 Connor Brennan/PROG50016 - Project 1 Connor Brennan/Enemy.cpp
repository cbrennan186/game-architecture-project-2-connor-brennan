#include "Enemy.h"

//This class contains all related data to the enemy ship


/// <summary>
/// A constructor for the enemy 
/// </summary>
Enemy::Enemy()
{

}

/// <summary>
/// A destructor for the enemy
/// </summary>
Enemy::~Enemy()
{

}

/// <summary>
/// Sets the value for the enemy dead bool
/// </summary>
/// <param name="_dead"></param>
void Enemy::SetEnemyDead(bool _dead)
{
	enemyDead = _dead;
}

/// <summary>
/// sets the value for the respawn counter for the enemy
/// </summary>
/// <param name="_counter"></param>
void Enemy::SetEnemyRespawnCounter(int _counter)
{
	enemyRespawnCounter = _counter;
}