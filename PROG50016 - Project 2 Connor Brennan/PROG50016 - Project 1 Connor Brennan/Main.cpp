#include <iostream>
#include <SFML/System.hpp>
#include<SFML/Graphics.hpp>
#include <string> 
#include <iostream>
#include <fstream>
#include "json.hpp"
#include "Window.h"

/// <summary>
/// The main class, calls rhe Initialize and RunGame functions which start and run the game.
/// </summary>
/// <returns></returns>
int main()
{
	Window* window = new Window();

	window->Initialize();
	window->RunGame();

	delete window;
}
