#include <iostream>
#include <SFML/System.hpp>
#include<SFML/Graphics.hpp>
#include <string> 
#include <iostream>
#include <fstream>
#include "json.hpp"
#include "Enemy.h"
#include "Player.h"
#include "Meteor.h"

/// <summary>
/// This is the header file for all window related data
/// </summary>
class Window
{
public:
	Window();
	~Window();
	void RunGame();
	void Initialize();

private:
	int windowXAxis;
	int windowYAxis;
	int characterSize;
	sf::RenderWindow* window;
	sf::View view;
	float halfwidth;
	float halfheight;
    sf::Texture backgroundTexture;
    sf::Texture playerTexture;
    sf::Texture enemyTexture;
    sf::Texture playerShotTexture;
    sf::Texture enemyShotTexture;
    sf::Texture playerDeadTexture;
    sf::Texture explostionTexture;
    sf::Texture smallMeteorTexture;
    sf::Texture largeMeteorTexture;
    sf::Texture heartTexture;
    sf::Texture enemyTexture2;
    sf::Sprite heartSprite;
    sf::Sprite heartSprite2;
    sf::Sprite heartSprite3;
    sf::Sprite backgroundSprite;
    sf::Sprite playerSprite;
    sf::Sprite enemySprite;
    sf::Sprite enemySprite2;
    sf::Sprite playerShotSprite;
    sf::Sprite enemyShotSprite;
    sf::Sprite smallMeteorSprite;
    sf::Sprite largeMeteorSprite;
    sf::Font font;
    sf::Text livesText;
    Player* player;
    Enemy* enemy;
    Meteor* meteor;
    sf::Text gameOverText;
    sf::Text scoreText;
    sf::Text highScoreText;
    sf::Text turnNumText;
    int playerShotCounter;
    int enemyShotCounter;
    bool shoot;
    bool playerLaserHitsEnemy;
    bool enemyLaserHitsPlayer;
    bool playerColideWithEnemy;
    bool enemyColideWithPlayer;
    bool smallAstroidHitsPlayer;
    bool largeAstroidHitsPlayer;
    bool playerLaserHitsSmallMeteor;
    bool playerLaserHitsLargeMeteor;
    bool ifGameOver;
    bool ifPlayerMoved;
    bool ifExitReached;
    int gameOverCounter;
    sf::Event event;
    sf::CircleShape playerHitCircle;
    sf::CircleShape enemyHitCircle;
    sf::CircleShape enemyHitCircle2;
    sf::CircleShape enemyShootCircle;
    sf::CircleShape playerShootCircle;
    sf::CircleShape smallMeteorCircle;
    sf::CircleShape largeMeteorCircle;
    sf::CircleShape exitCircle;
    int livesNum;
    std::string livesString;
    int scoreNum;
    std::string scoreString;
    int highscoreNum;
    std::string highscoreString;
    std::string turnNumString;
    bool playerDead;
    int playerRespawnCounter;
    bool enemyDead;
    bool enemyDead2;
    int enemyRespawnCounter;
    int turnsTaken;
    bool playerAcrossEnemy;
    bool playerAcrossEnemy2;
    int aiMovement;
    std::string errorMessage;
};

