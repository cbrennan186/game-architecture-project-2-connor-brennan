#include "Window.h"
#include <chrono>
#include <thread>

using namespace std::this_thread;
using namespace std::chrono; 

//In the window class all other classes, and the main game loop are created and run here.

//Constructor 
Window::Window()
{

}

//Destructor 
Window::~Window()
{
    delete window;
    delete player;
    delete enemy;
    delete meteor;
}

/// <summary>
/// Run on start up, initializes all the data to be ready to be used 
/// </summary>
void Window::Initialize()
{
    //Sets the window up
    windowXAxis = 1024;
    windowYAxis = 768;
    characterSize = 30;

    window = new sf::RenderWindow(sf::VideoMode(windowXAxis, windowYAxis), "Connor Brennans Dungeon Crawler");
    window->setFramerateLimit(60);

    halfwidth = window->getSize().x * 0.5;
    halfheight = window->getSize().y * 0.5;
    view.reset(sf::FloatRect(-halfwidth, -halfheight, halfwidth * 2.0, halfheight * 2.0));
    window->setView(view);

    //sets the textures up
    //backgroundTexture.loadFromFile("prog50049.project1.resources/Background/starBackground.png");
    backgroundTexture.loadFromFile("Assets/Game Resources/Dungeon/Tilemap.png");

    playerTexture.loadFromFile("Assets/Game Resources/Skeleton/Front/Front-Idle (1).png");

    heartTexture.loadFromFile("Assets/Game Resources/heart-image.png");

    enemyTexture.loadFromFile("Assets/Game Resources/Bat/Front/Front-Fly (1).png");
    
    enemyTexture2.loadFromFile("Assets/Game Resources/Witch/Front/Front-Attack (1).png");

    playerShotTexture.loadFromFile("prog50049.project1.resources/Mainplayer/laserGreen.png");

    enemyShotTexture.loadFromFile("prog50049.project1.resources/Enemies/laserRed.png");

    //heartTexture.loadFromFile("Assets/Game Resources/heart-image.png");

    //smallMeteorTexture.loadFromFile("prog50049.project1.resources/Asteroids/meteorSmall.png");
    //largeMeteorTexture.loadFromFile("prog50049.project1.resources/Asteroids/meteorBig.png");

    playerDeadTexture.loadFromFile("prog50049.project1.resources/Mainplayer/playerDamaged_2.png");

    //explostionTexture.loadFromFile("prog50049.project1.resources/Misc/exp2_0.png");

    //sets the sprites

    backgroundSprite.setTexture(backgroundTexture);
    backgroundSprite.setOrigin(sf::Vector2f(backgroundTexture.getSize().x * 0.5, backgroundTexture.getSize().y * 0.5));

    sf::Vector2f backgroundSize(windowXAxis, windowYAxis);

    backgroundSprite.setScale(
        backgroundSize.x / backgroundSprite.getLocalBounds().width,
        backgroundSize.y / backgroundSprite.getLocalBounds().height);

    playerSprite.setTexture(playerTexture);
    playerSprite.setOrigin(sf::Vector2f(playerTexture.getSize().x * 0.33, playerTexture.getSize().y * 0.72));

    heartSprite.setTexture(heartTexture);
    heartSprite.setOrigin(sf::Vector2f(heartTexture.getSize().x * 0.33, heartTexture.getSize().y * 0.72));

    heartSprite2.setTexture(heartTexture);
    heartSprite2.setOrigin(sf::Vector2f(heartTexture.getSize().x * 0.33, heartTexture.getSize().y * 0.72));

    heartSprite3.setTexture(heartTexture);
    heartSprite3.setOrigin(sf::Vector2f(heartTexture.getSize().x * 0.33, heartTexture.getSize().y * 0.72));

    enemySprite.setTexture(enemyTexture);
    enemySprite.setOrigin(sf::Vector2f(enemyTexture.getSize().x * 0.3, enemyTexture.getSize().y * 0.35));

    enemySprite2.setTexture(enemyTexture2);
    enemySprite2.setOrigin(sf::Vector2f(enemyTexture2.getSize().x * 0.3, enemyTexture2.getSize().y * 0.35));

    playerShotSprite.setTexture(playerShotTexture);
    playerShotSprite.setOrigin(sf::Vector2f(playerShotTexture.getSize().x * 0.0, playerShotTexture.getSize().y * 0.0));

    enemyShotSprite.setTexture(enemyShotTexture);
    enemyShotSprite.setOrigin(sf::Vector2f(enemyShotTexture.getSize().x * 0.0, enemyShotTexture.getSize().y * 0.0));

    smallMeteorSprite.setTexture(smallMeteorTexture);
    smallMeteorSprite.setOrigin(sf::Vector2f(smallMeteorTexture.getSize().x * 0.0, smallMeteorTexture.getSize().y * 0.0));

    largeMeteorSprite.setTexture(largeMeteorTexture);
    largeMeteorSprite.setOrigin(sf::Vector2f(largeMeteorTexture.getSize().x * 0.0, largeMeteorTexture.getSize().y * 0.0));

    //attempt at making menus
    //sf::Mouse::Button button;

    //if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
    //{
        
    //}
    // get global mouse position
    //sf::Vector2i position = sf::Mouse::getPosition();
    // set mouse position relative to a window
    //sf::Mouse::setPosition(sf::Vector2i(100, 200), window);

    if (font.loadFromFile("prog50049.project1.resources/Hud/cour.ttf") == false)
    {
        std::cout << "Could not find font" << std::endl;
    }

    //value setting
    livesNum = 3;
    scoreNum = 0;
    highscoreNum = 0;
    turnsTaken = 0;
    livesString = "Lives Left: ";
    scoreString = "Score: " + std::to_string(scoreNum);
    turnNumString = "Turns Taken: " + std::to_string(turnsTaken);

    //Text setting
    livesText.setFont(font);
    livesText.setCharacterSize(characterSize);
    livesText.setFillColor(sf::Color::Green);
    livesText.setStyle(sf::Text::Regular);
    livesText.setString(livesString);
    livesText.setOrigin(sf::Vector2f(livesText.getLocalBounds().width * 2.1f, livesText.getLocalBounds().height * 19.5f));

    gameOverText.setFont(font);
    gameOverText.setCharacterSize(characterSize);
    gameOverText.setFillColor(sf::Color::Green);
    gameOverText.setStyle(sf::Text::Regular);
    gameOverText.setString("Game Over");
    gameOverText.setOrigin(sf::Vector2f(gameOverText.getLocalBounds().width * 0.0f, gameOverText.getLocalBounds().height * 0.0f));

    scoreText.setFont(font);
    scoreText.setCharacterSize(characterSize);
    scoreText.setFillColor(sf::Color::Green);
    scoreText.setStyle(sf::Text::Regular);
    scoreText.setString(scoreString);
    scoreText.setOrigin(sf::Vector2f(scoreText.getLocalBounds().width * 2.82f, scoreText.getLocalBounds().height * 17.8f));

    turnNumString = ("Turns Taken: " + std::to_string(turnsTaken));

    turnNumText.setFont(font);
    turnNumText.setCharacterSize(30);
    turnNumText.setFillColor(sf::Color::Green);
    turnNumText.setStyle(sf::Text::Regular);
    turnNumText.setString(turnNumString);
    turnNumText.setOrigin(sf::Vector2f(turnNumText.getLocalBounds().width * 2.0f, turnNumText.getLocalBounds().height * 15.5f));

    std::ifstream inputStreamHighscore("./Assets/SaveData.json");
    std::string strHighscore((std::istreambuf_iterator<char>(inputStreamHighscore)), std::istreambuf_iterator<char>());
    json::JSON documentHighscore = json::JSON::Load(strHighscore);

    json::JSON subObjectHighscore = documentHighscore["subObjectPlayer"];

    //turnsTaken = (subObjectHighscore["highscoreNum"].ToInt());

    turnNumString = ("Turns Taken: " + std::to_string(turnsTaken));

    turnNumText.setString(turnNumString);

    playerShotCounter = 0;
    enemyShotCounter = 0;
    shoot = false;

    smallMeteorSprite.setPosition(150, -350);
    largeMeteorSprite.setPosition(-150, -350);
    playerSprite.setPosition(0, 230);
    heartSprite.setPosition(-500, -70);
    heartSprite2.setPosition(-450, -70);
    heartSprite3.setPosition(-400, -70);
    enemySprite.setPosition(0, -300);
    enemySprite2.setPosition(0, -150);

    enemyShotSprite.setPosition(enemySprite.getPosition().x + 20, enemySprite.getPosition().y);

    //hit circles setting
    sf::CircleShape _playerHitCircle(55);
    sf::CircleShape _enemyHitCircle(55);
    sf::CircleShape _enemyHitCircle2(55);
    sf::CircleShape _enemyShootCircle(10);
    sf::CircleShape _playerShootCircle(10);
    sf::CircleShape _smallMeteorCircle(10);
    sf::CircleShape _largeMeteorCircle(30);
    sf::CircleShape _exitCircle(30);

    playerHitCircle = _playerHitCircle;
    enemyHitCircle2 = _enemyHitCircle2;
    enemyHitCircle = _enemyHitCircle;
    enemyShootCircle = _enemyShootCircle;
    playerShootCircle = _playerShootCircle;
    smallMeteorCircle = _smallMeteorCircle;
    largeMeteorCircle = _largeMeteorCircle;
    exitCircle = _exitCircle;

    playerHitCircle.setFillColor(sf::Color(100, 250, 50));
    playerHitCircle.setPosition(playerSprite.getPosition().x, playerSprite.getPosition().y - 20.0);

    enemyHitCircle.setFillColor(sf::Color(100, 250, 50));
    enemyHitCircle.setPosition(enemySprite.getPosition().x, enemySprite.getPosition().y + -10.0);

    enemyHitCircle2.setFillColor(sf::Color(100, 250, 50));
    enemyHitCircle2.setPosition(enemySprite2.getPosition().x, enemySprite2.getPosition().y + 40.0);

    enemyShootCircle.setFillColor(sf::Color(100, 250, 50));
    enemyShootCircle.setPosition(enemyShotSprite.getPosition().x - 3, enemyShotSprite.getPosition().y);

    playerShootCircle.setFillColor(sf::Color(100, 250, 50));

    smallMeteorCircle.setFillColor(sf::Color(100, 250, 50));
    smallMeteorCircle.setPosition(smallMeteorSprite.getPosition().x, smallMeteorSprite.getPosition().y);

    largeMeteorCircle.setFillColor(sf::Color(100, 250, 50));
    largeMeteorCircle.setPosition(largeMeteorSprite.getPosition().x, largeMeteorSprite.getPosition().y);

    exitCircle.setFillColor(sf::Color(100, 250, 50));
    exitCircle.setPosition(330.0, -70.0);

    playerLaserHitsEnemy;
    enemyLaserHitsPlayer;
    playerColideWithEnemy;
    enemyColideWithPlayer;
    smallAstroidHitsPlayer;
    largeAstroidHitsPlayer;
    playerLaserHitsSmallMeteor;
    playerLaserHitsLargeMeteor;

    //Further value setting
    playerDead = false;
    enemyDead = false;
    enemyDead2 = false;

    ifGameOver = false;

    playerRespawnCounter = 90;
    enemyRespawnCounter = 90;

    gameOverCounter = 0;

    //This code, while it works, does not give the desired outcome. So has been left commented out.
    /*std::ifstream inputStream1("./Assets/PlayerData.json");
    std::string str1((std::istreambuf_iterator<char>(inputStream1)), std::istreambuf_iterator<char>());
    json::JSON document1 = json::JSON::Load(str1);

    std::ifstream inputStream2("./Assets/EnemyStartData.json");
    std::string str2((std::istreambuf_iterator<char>(inputStream2)), std::istreambuf_iterator<char>());
    json::JSON document2 = json::JSON::Load(str2);

    std::ifstream inputStream3("./Assets/WindowData.json");
    std::string str3((std::istreambuf_iterator<char>(inputStream3)), std::istreambuf_iterator<char>());
    json::JSON document3 = json::JSON::Load(str3);

    std::ifstream inputStream4("./Assets/SmallMeteorData.json");
    std::string str4((std::istreambuf_iterator<char>(inputStream4)), std::istreambuf_iterator<char>());
    json::JSON document4 = json::JSON::Load(str4);

    std::ifstream inputStream5("./Assets/LargeMeteorData.json");
    std::string str5((std::istreambuf_iterator<char>(inputStream5)), std::istreambuf_iterator<char>());
    json::JSON document5 = json::JSON::Load(str5);

    if (document3.hasKey("subObjectWindow"))
    {
        json::JSON subObject1 = document3["subObjectWindow"];
        if (subObject1.hasKey("windowXAxis"))
        {
            windowXAxis = subObject1["windowXAxis"].ToInt();
        }
        if (subObject1.hasKey("windowYAxis"))
        {
            windowYAxis = subObject1["windowYAxis"].ToInt();
        }
        if (subObject1.hasKey("ifGameOver"))
        {
            ifGameOver = subObject1["windowYAxis"].ToBool();
        }
        if (subObject1.hasKey("Font Characters Size"))
        {
            characterSize = subObject1["Font Characters Size"].ToInt();
        }
    }

    if (document1.hasKey("subObjectPlayer"))
    {
        json::JSON subObject2 = document1["subObjectPlayer"];
        if (subObject2.hasKey("livesNum"))
        {
            livesNum = subObject2["livesNum"].ToInt();

            livesString = "Lives Left: " + std::to_string(livesNum);

            livesText.setString(livesString);
        }
        if (subObject2.hasKey("scoreNum"))
        {
            scoreNum = subObject2["scoreNum"].ToInt();

            scoreString = "Score: " + std::to_string(scoreNum);

            scoreText.setString(scoreString);
        }
        if (subObject2.hasKey("PlayerPosX") && subObject2.hasKey("PlayerPosY"))
        {
            int x = subObject2["PlayerPosX"].ToInt();
            int y = subObject2["PlayerPosY"].ToInt();
            playerSprite.setPosition(x, y);
        }
        if (subObject2.hasKey("playerDead"))
        {
            playerDead = subObject2["playerDead"].ToBool();
        }
        if (subObject2.hasKey("playerRespawnCounter"))
        {
            playerRespawnCounter = subObject2["playerRespawnCounter"].ToInt();
        }
    }

    if (document2.hasKey("subObjectEnemy"))
    {
        json::JSON subObject3 = document2["subObjectEnemy"];
        if (subObject3.hasKey("EnemyPosX") && subObject3.hasKey("EnemyPosY"))
        {
            int x = subObject3["EnemyPosX"].ToInt();
            int y = subObject3["EnemyPosY"].ToInt();
            enemySprite.setPosition(x, y);
        }
        if (subObject3.hasKey("enemyDead"))
        {
            enemyDead = subObject3["enemyDead"].ToBool();
        }
        if (subObject3.hasKey("enemyRespawnCounter"))
        {
            enemyRespawnCounter = subObject3["enemyRespawnCounter"].ToInt();
        }
    }

    if (document4.hasKey("subObjectSmallMeteor"))
    {
        json::JSON subObject4 = document4["subObjectSmallMeteor"];
        if (subObject4.hasKey("smallMeteorPosX") && subObject4.hasKey("smallMeteorPosY"))
        {
            int x = subObject4["smallMeteorPosX"].ToInt();
            int y = subObject4["smallMeteorPosY"].ToInt();
            smallMeteorSprite.setPosition(x, y);
        }
    }

    if (document5.hasKey("subObjectLargeMeteor"))
    {
        json::JSON subObject5 = document5["subObjectLargeMeteor"];
        if (subObject5.hasKey("largeMeteorPosX") && subObject5.hasKey("largeMeteorPosY"))
        {
            int x = subObject5["largeMeteorPosX"].ToInt();
            int y = subObject5["largeMeteorPosY"].ToInt();
            largeMeteorSprite.setPosition(x, y);
        }
    }*/
}


//The main code for the game. Including the main loop. 
void Window::RunGame()
{
    //runs until the window is closed
    while (window != nullptr)
    {
        while (window != nullptr && window->pollEvent(event))
        {
            switch (event.type)
            {
                //if the game is over shut the whole game down 
            case sf::Event::Closed:
                window->close();
                delete window;
                window = nullptr;
                break;

                ifPlayerMoved = false;
                playerAcrossEnemy = playerHitCircle.getGlobalBounds().intersects(enemyHitCircle.getGlobalBounds());
                playerAcrossEnemy2 = playerHitCircle.getGlobalBounds().intersects(enemyHitCircle2.getGlobalBounds());

            case sf::Event::KeyPressed:
                //If the "A" key is pressed, move the character to the left
                if (event.key.code == sf::Keyboard::A && playerDead == false)
                {
                    playerSprite.move(-100, 0);
                    playerHitCircle.move(-100, 0);
                    ifPlayerMoved = true;
                    turnsTaken = turnsTaken + 1;
                    turnNumString = ("Turns Taken: " + std::to_string(turnsTaken));
                    turnNumText.setString(turnNumString);
                }
                //If the "D" key is pressed, move the character to the right
                else if (event.key.code == sf::Keyboard::D && playerDead == false)
                {
                    playerSprite.move(100, 0);
                    playerHitCircle.move(100, 0);
                    ifPlayerMoved = true;
                    turnsTaken = turnsTaken + 1;
                    turnNumString = ("Turns Taken: " + std::to_string(turnsTaken));
                    turnNumText.setString(turnNumString);
                }
                //If the "W" key is pressed, move the character up
                else if (event.key.code == sf::Keyboard::W && playerDead == false)
                {
                    playerSprite.move(0, -80);
                    playerHitCircle.move(0, -80);
                    ifPlayerMoved = true;
                    turnsTaken = turnsTaken + 1;
                    turnNumString = ("Turns Taken: " + std::to_string(turnsTaken));
                    turnNumText.setString(turnNumString);
                }
                //If the "S" key is pressed, move the character down
                else if (event.key.code == sf::Keyboard::S && playerDead == false)
                {
                    playerSprite.move(0, 80);
                    playerHitCircle.move(0, 80);
                    ifPlayerMoved = true;
                    turnsTaken = turnsTaken + 1;
                    turnNumString = ("Turns Taken: " + std::to_string(turnsTaken));
                    turnNumText.setString(turnNumString);
                }
                //If the left or right "Ctrl" key is pressed, add a point to the score and erase the enemy
                if (event.key.code == sf::Keyboard::LControl && playerDead == false
                    && playerAcrossEnemy == true && enemyDead == false ||
                    event.key.code == sf::Keyboard::RControl && playerDead == false
                    && playerAcrossEnemy == true && enemyDead == false)
                {
                    ifPlayerMoved = true;
                    scoreNum = (scoreNum + 1);
                    scoreString = ("Score: " + std::to_string(scoreNum));
                    scoreText.setString(scoreString);
                    enemyDead = true;
                    enemySprite.setTexture(enemyTexture);
                    enemySprite.setPosition(0, -300);
                    enemyHitCircle.setPosition(0, -300);
                }
                //If the left or right "Ctrl" key is pressed, add a point to the score and erase the enemy
                else if (event.key.code == sf::Keyboard::LControl && playerDead == false
                    && playerAcrossEnemy2 == true && enemyDead2 == false ||
                    event.key.code == sf::Keyboard::RControl && playerDead == false
                    && playerAcrossEnemy2 == true && enemyDead2 == false)
                {
                    ifPlayerMoved = true;
                    scoreNum = (scoreNum + 1);
                    scoreString = ("Score: " + std::to_string(scoreNum));
                    scoreText.setString(scoreString);
                    enemyDead2 = true;
                    enemySprite2.setTexture(enemyTexture);
                    enemySprite2.setPosition(0, -300);
                    enemyHitCircle2.setPosition(0, -300);
                }
                //If the "Esc" key is pressed, close the game
                else if (event.key.code == sf::Keyboard::Escape)
                {
                    window = nullptr;
                }
                //If the L key is pressed load the data from the file
                else if (event.key.code == sf::Keyboard::L)
                {
                    std::ifstream inputStream("./Assets/SaveData.json");
                    std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
                    json::JSON document = json::JSON::Load(str);

                    if (document.hasKey("subObjectWindow"))
                    {
                        json::JSON subObject = document["subObjectWindow"];
                        if (subObject.hasKey("windowXAxis"))
                        {
                            windowXAxis = subObject["windowXAxis"].ToInt();
                        }
                        if (subObject.hasKey("windowYAxis"))
                        {
                            windowYAxis = subObject["windowYAxis"].ToInt();
                        }
                        if (subObject.hasKey("ifGameOver"))
                        {
                            ifGameOver = subObject["windowYAxis"].ToBool();
                        }
                        if (subObject.hasKey("Font Characters Size"))
                        {
                            characterSize = subObject["Font Characters Size"].ToInt();
                        }
                    }

                    if (document.hasKey("subObjectPlayer"))
                    {
                        json::JSON subObject = document["subObjectPlayer"];
                        if (subObject.hasKey("livesNum"))
                        {
                            livesNum = (subObject["livesNum"].ToInt());

                            livesString = ("Lives Left: ");

                            livesText.setString(livesString);
                        }
                        if (subObject.hasKey("scoreNum"))
                        {
                            scoreNum = (subObject["scoreNum"].ToInt());

                            scoreString = ("Score: " + std::to_string(scoreNum));

                            scoreText.setString(scoreString);
                        }
                        if (subObject.hasKey("highscoreNum"))
                        {
                            highscoreNum = (subObject["highscoreNum"].ToInt());

                            highscoreString = ("High score: " + std::to_string(highscoreNum));

                            highScoreText.setString(highscoreString);
                        }
                        if (subObject.hasKey("PlayerPosX") && subObject.hasKey("PlayerPosY"))
                        {
                            int x = subObject["PlayerPosX"].ToInt();
                            int y = subObject["PlayerPosY"].ToInt();
                            playerSprite.setPosition(x, y);
                        }
                        if (subObject.hasKey("playerDead"))
                        {
                            playerDead = (subObject["playerDead"].ToBool());
                        }
                        if (subObject.hasKey("playerRespawnCounter"))
                        {
                            playerRespawnCounter = (subObject["playerRespawnCounter"].ToInt());
                        }
                    }

                    if (document.hasKey("subObjectEnemy"))
                    {
                        json::JSON subObject = document["subObjectEnemy"];
                        if (subObject.hasKey("EnemyPosX") && subObject.hasKey("EnemyPosY"))
                        {
                            int x = subObject["EnemyPosX"].ToInt();
                            int y = subObject["EnemyPosY"].ToInt();
                            enemySprite.setPosition(x, y);
                            enemySprite2.setPosition(x, y);
                        }
                        if (subObject.hasKey("enemyDead"))
                        {
                            enemyDead = (subObject["enemyDead"].ToBool());
                        }
                        if (subObject.hasKey("enemyRespawnCounter"))
                        {
                            enemyRespawnCounter = (subObject["enemyRespawnCounter"].ToInt());
                        }
                    }

                    if (document.hasKey("subObjectSmallMeteor"))
                    {
                        json::JSON subObject = document["subObjectSmallMeteor"];
                        if (subObject.hasKey("smallMeteorPosX") && subObject.hasKey("smallMeteorPosY"))
                        {
                            int x = subObject["smallMeteorPosX"].ToInt();
                            int y = subObject["smallMeteorPosY"].ToInt();
                            smallMeteorSprite.setPosition(x, y);
                        }
                    }

                    if (document.hasKey("subObjectLargeMeteor"))
                    {
                        json::JSON subObject = document["subObjectLargeMeteor"];
                        if (subObject.hasKey("largeMeteorPosX") && subObject.hasKey("largeMeteorPosY"))
                        {
                            int x = subObject["largeMeteorPosX"].ToInt();
                            int y = subObject["largeMeteorPosY"].ToInt();
                            largeMeteorSprite.setPosition(x, y);
                        }
                    }
                }
                //If the "~" key is pressed, save the game data to a file
                else if (event.key.code == sf::Keyboard::Tilde)
                {
                    json::JSON document;

                    json::JSON subObjectWindow = json::JSON::Object();
                    subObjectWindow["windowXAxis"] = windowXAxis;
                    subObjectWindow["windowYAxis"] = windowYAxis;
                    subObjectWindow["ifGameOver"] = ifGameOver;
                    subObjectWindow["Font Characters Size"] = characterSize;
                    document["subObjectWindow"] = subObjectWindow;

                    json::JSON subObjectPlayer = json::JSON::Object();
                    subObjectPlayer["livesNum"] = livesNum;
                    subObjectPlayer["scoreNum"] = scoreNum;
                    subObjectPlayer["highscoreNum"] = highscoreNum;
                    subObjectPlayer["PlayerPosX"] = playerSprite.getPosition().x;
                    subObjectPlayer["PlayerPosY"] = playerSprite.getPosition().y;
                    subObjectPlayer["playerDead"] = playerDead;
                    subObjectPlayer["playerRespawnCounter"] = playerRespawnCounter;
                    document["subObjectPlayer"] = subObjectPlayer;

                    json::JSON subObjectEnemy = json::JSON::Object();
                    subObjectEnemy["EnemyPosX"] = enemySprite.getPosition().x;
                    subObjectEnemy["EnemyPosY"] = enemySprite.getPosition().y;
                    subObjectEnemy["enemyDead"] = enemyDead;
                    subObjectEnemy["enemyRespawnCounter"] = enemyRespawnCounter;
                    document["subObjectEnemy"] = subObjectEnemy;

                    json::JSON subObjectSmallMeteor = json::JSON::Object();
                    subObjectSmallMeteor["smallMeteorPosX"] = smallMeteorSprite.getPosition().x;
                    subObjectSmallMeteor["smallMeteorPosY"] = smallMeteorSprite.getPosition().y;
                    document["subObjectSmallMeteor"] = subObjectSmallMeteor;

                    json::JSON subObjectLargeMeteor = json::JSON::Object();
                    subObjectLargeMeteor["largeMeteorPosX"] = largeMeteorSprite.getPosition().x;
                    subObjectLargeMeteor["largeMeteorPosY"] = largeMeteorSprite.getPosition().y;
                    document["subObjectLargeMeteor"] = subObjectLargeMeteor;

                    std::ofstream ostrm("./Assets/SaveData.json");
                    ostrm << document.dump() << std::endl;
                }
                break;

            default:
                break;
            }
        }

        //After the player moves, allow the first enemy to act once
        if (enemyDead == false && ifPlayerMoved == true)
        {
            playerAcrossEnemy = playerHitCircle.getGlobalBounds().intersects(enemyHitCircle.getGlobalBounds());

            //If the enemy comes close enough to the player, attack the player, removing a heart
            if (playerAcrossEnemy == true && playerDead == false)
            {
                livesNum = (livesNum - 1);
                livesString = ("Lives Left: ");
                livesText.setString(livesString);
                if (livesNum == 0)
                {
                    playerDead = true;
                }
            }

            //sleep_for(seconds(3));
            enemySprite.move(0, 80);
            enemyHitCircle.move(0, 80);
            //ifPlayerMoved = false;
        }

        if (enemyDead2 == false && ifPlayerMoved == true)
        {
            playerAcrossEnemy2 = playerHitCircle.getGlobalBounds().intersects(enemyHitCircle2.getGlobalBounds());

            if (playerAcrossEnemy2 == true && playerDead == false)
            {
                livesNum = (livesNum - 1);
                livesString = ("Lives Left: ");
                livesText.setString(livesString);
                if (livesNum == 0)
                {
                    playerDead = true;
                }
            }

            //sleep_for(seconds(3));

            aiMovement = rand() % 4 + 1;

            //move the second enemy depending on the number generated
            if (aiMovement == 1)
            {
                enemySprite2.move(0, 80);
                enemyHitCircle2.move(0, 80);
            }
            else if (aiMovement == 2)
            {
                enemySprite2.move(0, -80);
                enemyHitCircle2.move(0, -80);
            }
            else if (aiMovement == 3)
            {
                enemySprite2.move(100, 0);
                enemyHitCircle2.move(100, 0);
            }
            else if (aiMovement == 4)
            {
                enemySprite2.move(-100, 0);
                enemyHitCircle2.move(-100, 0);
            }
            else 
            {
                errorMessage = "Something went wrong in the random number generator.";
            }

            ifPlayerMoved = false;
        }

        //If the window is not closed, display all the sprites and textures needed
        if (window != nullptr) {

            window->clear();

            //The commented out code below is to show the hit circles, 
            //i have left them here in case you wanted to see them. 
            window->draw(backgroundSprite);
            window->draw(exitCircle);

            //window->draw(playerHitCircle);

            //window->draw(enemyHitCircle);
            
            //window->draw(enemyHitCircle2);
            
            //Only if the enemies arent dead, should they be displayed
            if (enemyDead == false)
            {
                window->draw(enemySprite);
            }

            if (enemyDead2 == false)
            {
                window->draw(enemySprite2);
            }

            window->draw(playerSprite);

            //Changes the number of hearts shown depending on how much health the player has left
            if (livesNum == 3)
            {
                window->draw(heartSprite);
                window->draw(heartSprite2);
                window->draw(heartSprite3);
            }
            else if (livesNum == 2)
            {
                window->draw(heartSprite);
                window->draw(heartSprite2);
            }
            else if (livesNum == 1)
            {
                window->draw(heartSprite);
            }
            else
            {

            }


            //Displays the text
            window->draw(livesText);
            window->draw(scoreText);
           window->draw(turnNumText);

           //If the player runs out of health, display the game over message
            if (livesNum <= 0)
            {
                window->draw(gameOverText);
                ifGameOver = true;
            }

            //Make the game wait a period of time after the player looses 
            //to allow them to read what happened. Then close the game.
            if (livesNum <= 0 && ifGameOver == true)
            {
                gameOverCounter = gameOverCounter + 1;
                if (gameOverCounter == 75)
                {
                    break;
                }

            }

            //If the player reached the exit, display the game over message and pause the game for 3 seconds.
            //Then close the game.
            ifExitReached = playerHitCircle.getGlobalBounds().intersects(exitCircle.getGlobalBounds());
            if (ifExitReached == true)
            {
                window->draw(gameOverText);
                ifGameOver = true;
                window->display();
                sleep_for(seconds(3));
                break;
            }

            window->display();
        }
    }
}